import mongoose, { Schema } from 'mongoose';

const schema = new Schema({
  name: {
    type: String,
    required: true,
  },
  connectionStrength: {
    type: Number,
    required: false,
  },
  isListening: {
    type: Boolean,
    required: true,
    default: false,
  },
  isAvailable: {
    type: Boolean,
    default: true,
  },
  currentTrack: {
    type: Object,
    default: null,
  },
  currentTrackStartedAt: {
    type: Number,
    default: null,
  },
  pincode: {
    type: String,
    required: true,
  },
});

export default mongoose.model('Room', schema);
