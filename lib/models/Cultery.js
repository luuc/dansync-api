import mongoose, { Schema } from 'mongoose';

// cutlery will become 'conntection'
const schema = new Schema({
  name: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  kind: {
    type: String,
    required: true,
  },
  connectionStrength: {
    type: Number,
    required: true,
  },
});

export default mongoose.model('Cultery', schema);
