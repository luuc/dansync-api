import mongoose, { Schema } from 'mongoose';

const schema = new Schema({
  vote: {
    type: String,
    required: true,
    enum: ['💩', '👏', '🤸‍♂️', '🕺'],
  },
  roomId: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  songId: {
    type: String,
    required: true,
  },
  timestamp: {
    type: Number,
    required: true,
  },
  revote: {
    type: Boolean,
    required: true,
    default: false,
  },
});

export default mongoose.model('Vote', schema);
