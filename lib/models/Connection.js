import mongoose, { Schema } from 'mongoose';

// cutlery will become 'conntection'
const schema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  roomId: {
    type: String,
    required: true,
  },
  timestamp: {
    type: Number,
    required: true,
  },
  playlist: {
    type: String,
    required: false,
  },
  isActive: {
    type: Boolean,
    required: true,
    default: false,
  },
  deviceId: {
    type: String,
    default: null,
  },
});

export default mongoose.model('Connection', schema);
