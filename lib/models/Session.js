import mongoose, { Schema } from 'mongoose';

const schema = new Schema({
  accessToken: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
});

export default mongoose.model('Session', schema);
