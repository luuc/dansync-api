import mongoose, { Schema } from 'mongoose';

const schema = new Schema({
  roomId: {
    type: String,
    required: true,
  },
  loudness: {
    type: Number,
  },
  energy: {
    type: Number,
  },
  danceability: {
    type: Number,
  },
  acousticness: {
    type: Number,
  },
  timestamp: {
    type: Number,
  },
});

export default mongoose.model('Meta', schema);
