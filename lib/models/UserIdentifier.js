import mongoose, { Schema } from 'mongoose';

const schema = new Schema({
  accessToken: {
    type: String,
    required: true,
  },
  refreshToken: {
    type: String,
    required: true,
  },
  email: {
    type: String,
  },
  grantingTimestamp: {
    type: Number,
    required: true,
    default: 0,
  },
  spotifyId: {
    type: Schema.Types.Mixed,
    required: true,
  },
});

export default mongoose.model('UserIdentifier', schema);
