import SocketIO from 'socket.io';
import Boom from 'boom';
import Vote from '../models/Vote';

import votingService from '../services/voting';
import systemService from '../services/system';
import userService from '../services/user';

export default {
  _httpServer: null,
  _socketServer: null,

  _clients: {},

  initializeWithHttpServer(httpServer) {
    this._httpServer = httpServer;

    this._socketServer = new SocketIO(this._httpServer, {
      origins: '*:*',
      serveClient: false,
      pingInterval: 15000,
      pingTimeout: 30000,
    });

    this._setupEvents();
  },

  _setupEvents() {
    this._socketServer.on('connection', async (socket) => {
      socket.on('login', async (data) => {
        try {
          const { accessToken, roomId } = data;
          const { user } = await userService.findOneByAccessToken(accessToken);

          if (user == null) {
            Boom.notFound();
          }

          const userProfile = await userService.getMeWithFrontData(user);

          // TODO
          this._clients = Object.assign(this._clients, {
            [roomId]: this._clients[roomId] == null ? [socket] : [...this._clients[roomId], socket],
          });


          this._emitToRoomWithId(roomId, 'newUser', userProfile);
          socket.emit('loginSuccess', { status: 201 });
        } catch (err) {
          // 'error' is a reserved event by socket.io, therefore we use 'customError'
          socket.emit('customError', err.output.payload);
          socket.disconnect();
        }
      });

      socket.on('vote', async (data) => {
        try {
          const { accessToken, roomId, songId, vote } = data;

          const { user } = await userService.findOneByAccessToken(accessToken);
          if (user == null) {
            Boom.notFound();
          }

          let voteDoc = await Vote
            .findOne({ userId: user.id, roomId, songId, timestamp: { $gt: Math.floor(new Date() / 1000) - 600 } })
            .exec();

          if (voteDoc != null) {
            voteDoc.vote = vote;
            voteDoc.timestamp = Math.floor(new Date() / 1000);
            voteDoc.revote = true;
            voteDoc.save();
          } else {
            voteDoc = await Vote.create({
              userId: user.id,
              roomId,
              songId,
              vote,
              timestamp: Math.floor(new Date() / 1000),
            });
          }

          if (await votingService.shouldSkipTrack(roomId, songId) === true) {
            systemService.triggerNextSong(roomId, 0);
          }

          this._emitToRoomWithId(roomId, 'vote', voteDoc);
        } catch (err) {
          console.log(err);
          // 'error' is a reserved event by socket.io, therefore we use 'customError'
          socket.emit('customError', err);
          socket.disconnect();
        }
      });

      socket.on('disconnect', () => {
        console.log('disconnecting...');
        // this._removeSocketFromClients(socket);
      });
    });
  },

  _emitToRoomWithId(roomId, key, value) {
    if (this._clients[roomId] == null) {
      return;
    }

    this._clients[roomId].forEach((socket) => {
      socket.emit(key, value);
    });
  },
};
