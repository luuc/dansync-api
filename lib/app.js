import dotenv from 'dotenv';
import Boom from 'boom';
import Promise from 'bluebird';

import express from 'express';
import http from 'http';
import forceSSL from 'force-ssl-heroku';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import mongoose from 'mongoose';

import state from './state';

import authApiController from './controllers/auth';
import roomApiController from './controllers/room';
import spotifyApiController from './controllers/spotify';
import connectionApiController from './controllers/connection';
import metaApiController from './controllers/meta';
import roomSocket from './sockets/room';

dotenv.config();

if (state.isServerMocked !== true) {
  mongoose.Promise = Promise;
  mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
}

const app = express();

app.use(forceSSL);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '20mb' }));
app.use(cookieParser());

app.use(express.static('public'));

/**
 * Home Routes & Plugins
 */
app.all('/*', (req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT');
  next();
});

app.get('/', (req, res) => {
  res.json({
    title: 'Musical Cultery API',
    version: 1.0,
  });
});

const apiRouter = new express.Router();
authApiController(apiRouter);
roomApiController(apiRouter);
spotifyApiController(apiRouter);
connectionApiController(apiRouter);
metaApiController(apiRouter);


/**
 * Disable cache for all API routes
 */
app.use('/api/v1', (req, res, next) => {
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', '0');

  next();
});

app.use('/v1/', apiRouter);

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});

/**
 * Error Handling
 */

/* eslint-disable no-unused-vars */
export const apiErrorHandler = (err, req, res, next) => {
  console.log(err);

  const parseError = () => {
    if (err.name === 'ValidationError') {
      const firstKey = Object.keys(err.errors)[0];
      return Boom.badRequest(err.errors[firstKey]);
    }

    if (err.isBoom === true) {
      return err;
    }
    return Boom.badImplementation();
  };

  const error = parseError();
  res.status(error.output.statusCode).json(error.output.payload);
};


app.use('/v1/', apiErrorHandler);


const port = process.env.PORT || 3000;
const server = http.createServer(app);

roomSocket.initializeWithHttpServer(server);

server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

export default app;
