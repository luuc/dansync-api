import axios from 'axios';
import Boom from 'boom';

import systemService from '../services/system';
import UserIdentifier from '../models/UserIdentifier';
import Session from '../models/Session';

export default {
  async getMe(accessToken) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me',
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest(e);
    }

    return data.data;
  },
  async getMeWithFrontData(user) {
    const userProfile = await this.getMe(user.accessToken);
    userProfile.mcId = user.id;
    userProfile.position = {
      top: Math.floor(Math.random() * 80) + 1,
      left: Math.floor(Math.random() * 80) + 1,
      animTime: Math.random() * 3 + 1,
      xMin: Math.random() >= 0.5 && '-',
      xVal: Math.floor(Math.random() * 30) + 1,
      yMin: Math.random() >= 0.5 && '-',
      yVal: Math.floor(Math.random() * 30) + 1,
    };

    return userProfile;
  },

  async getUserById(userId) {
    return UserIdentifier.findOne({ _id: userId }).exec();
  },

  async findOneByAccessToken(accessToken) {
    const session = await Session.findOne({ accessToken }).exec();
    if (session == null) {
      throw Boom.unauthorized();
    }

    const user = await UserIdentifier.findOne({ _id: session.userId }).exec();
    if (user == null) {
      throw Boom.unauthorized();
    }
    return { user, session };
  },
};
