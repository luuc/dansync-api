import base64 from 'base-64';
import spotifyService from './spotify';

export default {
  getAuthHeader(accessToken = null) {
    if (accessToken == null) { return `Basic ${base64.encode(`${process.env.CLIENT_ID}:${process.env.CLIENT_SECRET}`)}`; }

    return `Bearer ${accessToken}`;
  },

  triggerNextSong(roomId, customDelay = false) {
    if (customDelay === 0) {
      spotifyService.playNextSongForRoom(roomId);
      return;
    }
    setTimeout(() => { spotifyService.playNextSongForRoom(roomId); }, process.env.DURATION * 1000);
  },
};
