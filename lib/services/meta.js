import Meta from '../models/Meta';
import roomService from './rooms';
import userService from './user';
import spotifyService from './spotify';

export default {
  async getNextSongForRoom(roomId) {
    const userIds = await roomService.getRoomUserIds(roomId);
    const someUser = await userService.getUserById(userIds[0]);
    const songs = await spotifyService.getPossibleRoomSongs(userIds, roomId);
    const songIds = await songs.map((song) => {
      return song.id;
    });
    const analysedSongs = await spotifyService.getSongAnalysis(someUser.accessToken, songIds);

    const isInSync = await this.isInSync();

    let selectedSong = null;
    if (isInSync === true) {
      selectedSong = await this.pickASong(roomId, analysedSongs);
    } else {
      selectedSong = await this.pickRandomSong(songs);
    }

    return {
      songUri: selectedSong.uri,
      ownerId: someUser.id,
      userIds,
    };
  },
  pickRandomSong(songs) {
    console.log('picked random song...');
    return songs[Math.floor(Math.random() * songs.length)];
  },
  async isInSync() {
    const lastMeta = await Meta.find().sort({ _id: -1 }).limit(2).exec();
    if (Math.abs(lastMeta[0].energy - lastMeta[1].energy) < 1) {
      return true;
    }
    return false;
  },

  async pickASong(roomId, analysedSongs) {
    const metas = await this.getRoomMetasInScope(roomId, 20000);

    if (metas.length < 1) {
      return this.pickRandomSong(analysedSongs);
    }

    const metasSummed = await metas.reduce((acc, meta) => {
      return {
        energy: acc.energy + meta.energy,
        danceability: acc.danceability + meta.danceability,
        acousticness: acc.acousticness + meta.acousticness,
        count: acc.count + 1,
      };
    }, { energy: 0, danceability: 0, acousticness: 0, count: 0 });

    const metasAveraged = {
      energy: metasSummed.energy / metasSummed.count,
      danceability: metasSummed.danceability / metasSummed.count,
      acousticness: metasSummed.acousticness / metasSummed.count,
    };

    const normalizedMetas = this.normalizeMetas(metasAveraged);
    const bestAttribute = await this.pickBestAttribute(normalizedMetas);
    const sortable = analysedSongs.audio_features;

    if (bestAttribute.value < 0.5) {
      // get highest attribute by value
      await sortable.sort((a, b) => {
        return a[bestAttribute.type] - b[bestAttribute.type];
      });
    } else {
      await sortable.sort((a, b) => {
        return b[bestAttribute.type] - a[bestAttribute.type];
      });
    }

    const pickedSong = sortable[Math.floor(Math.random() * 5)];
    console.log(bestAttribute, pickedSong);
    return pickedSong;
  },

  async pickBestAttribute(metas) {
    const sortable = [
      [Object.keys(metas)[0], metas[Object.keys(metas)[0]]],
      [Object.keys(metas)[1], metas[Object.keys(metas)[1]]],
      [Object.keys(metas)[2], metas[Object.keys(metas)[2]]],
    ];

    await sortable.sort((a, b) => {
      return Math.abs(a[1] - 0.5) - Math.abs(b[1] - 0.5);
    });

    return {
      type: sortable[2][0],
      value: sortable[2][1],
    };
  },

  normalizeMetas(metas) {
    const energy = (metas.energy / 3.5) > 1 ? 1 : metas.energy / 3.5;
    const danceability = (metas.danceability / 2) > 1 ? 1 : metas.danceability / 2;
    let acousticness = (metas.acousticness / 2) > 1 ? 1 : metas.acousticness / 2;


    if (acousticness > 0.5 && danceability < 0.3) {
      acousticness = 1;
    }

    if (danceability > 0.3) {
      acousticness = 0.5;
    }

    return {
      energy,
      danceability,
      acousticness,
    };
  },

  async getRoomMetasInScope(roomId, scope) {
    const now = Math.floor(new Date() / 1000);
    return Meta.find({
      roomId,
      timestamp: {
        $gt: now - (scope / 1000),
        $lt: now,
      },
    }).exec();
  },
};
