import Vote from '../models/Vote';
import Connection from '../models/Connection';

export default {
  async shouldSkipTrack(roomId, songId) {
    // We check for same song in last 10 minutes: assuming song is not the same and less than 10 mins
    const tenMinutesAgo = Math.floor(new Date() / 1000) - 600;
    const downVotes = await Vote
      .find({ roomId, songId, vote: '💩', timestamp: { $gt: tenMinutesAgo } })
      .distinct('userId');

    const usersInRoom = await Connection.find({ roomId, isActive: true }).exec();

    return downVotes.length > Math.floor(usersInRoom.length * 0.5);
  },
};
