import spotifyService from './spotify';

export default {
  _timers: {},

  setTimer(roomId, timeout) {
    if (this._timers[roomId] != null) {
      clearTimeout(this._timers[roomId]);
    }

    this._timers[roomId] = setTimeout(() => {
      spotifyService.playNextSongForRoom(roomId);
    }, timeout);
  },
};
