import Boom from 'boom';

import Room from '../models/Room';
import Cultery from '../models/Cultery';
import UserIdentifier from '../models/UserIdentifier';
import Connection from '../models/Connection';
import spotifyService from './spotify';

export default {
  async getRoomById(id) {
    let room;

    try {
      room = await Room.findOne({ _id: id }).exec();
    } catch (e) {
      throw Boom.badRequest('getRoomById()');
    }

    return room;
  },

  async getRoomCulteryById(roomId, culteryId) {
    let cultery;

    try {
      cultery = await Cultery.findOne({ _id: culteryId, roomId }).exec();
    } catch (e) {
      throw Boom.badRequest('getRoomCulteryById()');
    }

    return cultery;
  },

  async getRoomConnections(roomId) {
    return Connection.find({ roomId }).exec();
  },

  async getRoomUserIds(roomId) {
    const connections = await Connection.find({ roomId }).exec();
    return connections.map((connection) => { return connection.userId; });
  },

  async getRoomUsers(roomId) {
    const userIds = await this.getRoomUserIds(roomId);
    return UserIdentifier.find({ _id: { $in: userIds } }).exec();
  },

  async playCurrentRoomTrack(accessToken, room) {
    const { uri, duration_ms } = room.currentTrack.tracks[0];

    const now = (new Date()).getTime();
    const position = now - room.currentTrackStartedAt;

    await spotifyService.setNextSong(uri, accessToken);

    if (position < duration_ms) {
      await spotifyService.seekTo(accessToken, position);
    }
  },

  async connectUserToRoom(userId, roomId) {
    const connection = await Connection.findOne({ userId, roomId }).exec();

    if (connection != null) {
      connection.isActive = true;
      return connection.save();
    }

    if (connection == null) {
      return Connection.create({
        roomId,
        userId,
        timestamp: Math.floor(new Date() / 1000),
        isActive: true,
      });
    }
  },
};
