import axios from 'axios';
import Boom from 'boom';
import queryString from 'querystring';

import systemService from '../services/system';
import metaService from '../services/meta';
import userService from '../services/user';
import roomService from '../services/rooms';
import Connection from '../models/Connection';

import RoomSocket from '../sockets/room';
import rooms from '../services/rooms';
import RoomManager from './RoomManager';

export default {
  async getDiscoverWeekly(accessToken) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me/top/tracks',
        params: {
          limit: 25,
          // time_range: 'long_range',
        },
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting top tracks (spotify)', e.response.data.error);
    }
    return data.data;
  },

  async getSongDetails(accessToken, songIds) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/tracks',
        params: {
          ids: songIds.join(','),
        },
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting track details (spotify)', e.response.data.error);
    }
    return data.data;
  },

  async getSongAnalysis(accessToken, songIds) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/audio-features',
        params: {
          ids: songIds.join(','),
        },
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting track details (spotify)', e.response.data.error);
    }
    console.log(data);
    return data.data;
  },

  async setPlayer(accessToken, deviceId) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me/player',
        method: 'put',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
        data: {
          device_ids: [deviceId],
          play: true,
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble setting device connection (spotify)', e.response.data.error);
    }

    if (data.status !== 204) { throw Boom.badImplementation(); }

    return data;
  },

  async getPlayers(accessToken) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me/player/devices',
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting active devices (spotify)', e.response.data.error);
    }

    return data.data.devices;
  },

  async setNextSong(songUri, accessToken) {
    if (songUri == null) { throw Boom.internal(); }

    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me/player/play',
        method: 'put',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
        data: {
          uris: [songUri],
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble setting next song (spotify)', e.response.data.error);
    }

    if (data.status !== 204) { throw Boom.badImplementation(); }

    return 204;
  },

  async playNextSongForRoom(roomId) {
    console.log('playnextsongforroom');
    const room = await roomService.getRoomById(roomId);

    if (room == null) { throw Boom.notFound(); }

    room.isListening = false;
    room.save();

    const { songUri, userIds, ownerId } = await metaService.getNextSongForRoom(room.id);

    // play song for each user
    const resPromises = userIds.map(async (userId) => {
      const user = await userService.getUserById(userId);
      return this.setNextSong(songUri, user.accessToken);
    });

    const startedAt = (new Date()).getTime();
    const owner = await userService.getUserById(ownerId);
    const track = await this.getSongDetails(owner.accessToken, [songUri.split(':')[2]]);
    track.tracks[0].startedAt = startedAt;

    room.currentTrack = track;
    room.currentTrackStartedAt = startedAt;
    // todo add owner
    room.save();

    RoomManager.setTimer(roomId, track.tracks[0].duration_ms);

    RoomSocket._emitToRoomWithId(roomId, 'newOwner', { ownerId });
    RoomSocket._emitToRoomWithId(roomId, 'newTrack', track);

    const res = await Promise.all(resPromises);

    return res;
  },

  async aggregateDiscoverWeekly(userIds) {
    const getDiscoverWeeklyPromises = userIds.map(async (userId) => {
      const user = await userService.getUserById(userId);
      let discoverWeekly;

      try {
        discoverWeekly = await this.getDiscoverWeekly(user.accessToken);
      } catch (e) {
        return null;
      }

      return discoverWeekly.items;
    });

    const discoverWeeklys = await Promise.all(getDiscoverWeeklyPromises);

    return discoverWeeklys.reduce((prev, group) => { return prev.concat(group); }, []);
  },

  async getPossibleRoomSongs(userIds, roomId) {
    const possibleRoomSongsPromise = userIds.map(async (userId) => {
      const user = await userService.getUserById(userId);
      const connection = await Connection.findOne({ userId, roomId }).exec();
      let discoverWeekly;
      let songs;

      // user hasn't chosen his playlist
      if (connection.playlist == null) {
        try {
          discoverWeekly = await this.getDiscoverWeekly(user.accessToken);
        } catch (e) {
          console.log(e);
          return null;
        }

        return discoverWeekly.items.map((item) => { return { ...item, owner: userId }; });
      }


      try {
        songs = await this.getSongsFromPlaylist(user.accessToken, connection.playlist);
      } catch (e) {
        return null;
      }

      return songs.items.reduce((prev, group) => { return prev.concat({ ...group.track, owner: userId }); }, []);
    });

    const possibleRoomSongs = await Promise.all(possibleRoomSongsPromise);
    return possibleRoomSongs.reduce((prev, group) => { return prev.concat(group); }, []);
  },

  async getUserPlaylists(accessToken) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me/playlists?limit=50',
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting songs from playlist (spotify)', e.response.data.error);
    }
    return data.data;
  },
  async getSongsFromPlaylist(accessToken, playlistId) {
    let data;
    try {
      data = await axios({
        url: `https://api.spotify.com/v1/playlists/${playlistId}/tracks`,
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting songs from playlist (spotify)', e.response.data.error);
    }
    return data.data;
  },

  async getInfoFromPlaylist(accessToken, playlistId) {
    let data;
    try {
      data = await axios({
        url: `https://api.spotify.com/v1/playlists/${playlistId}`,
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting info from playlist (spotify)', e.response.data.error);
    }
    return data.data;
  },

  async getHistory(accessToken) {
    let data;
    try {
      data = await axios({
        url: 'https://api.spotify.com/v1/me/player/recently-played',
        params: {
          limit: 10,
        },
        method: 'get',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble getting lastest songs (spotify)', e.response.data.error);
    }
    return data.data;
  },

  getRedirect(state) {
    const SCOPE = 'streaming user-read-email user-read-private user-top-read user-read-playback-state user-modify-playback-state playlist-read-private playlist-read-collaborative';

    return {
      redirect: `https://accounts.spotify.com/authorize?${
        queryString.stringify({
          response_type: 'code',
          client_id: process.env.CLIENT_ID,
          scope: SCOPE,
          redirect_uri: `${process.env.WEB}/authorization`,
          state,
        })}`,
    };
  },
  async refreshToken(user) {
    const mongoUser = user;
    let spotifyAuth;

    try {
      spotifyAuth = await axios({
        url: 'https://accounts.spotify.com/api/token',
        method: 'post',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: systemService.getAuthHeader(),
        },
        params: {
          grant_type: 'refresh_token',
          refresh_token: user.refreshToken,
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble refreshing accessToken (spotify)', e.response.data.error);
    }

    if (spotifyAuth.data.access_token == null) { throw Boom.badRequest('Cannot get spotify access'); }

    mongoUser.accessToken = spotifyAuth.data.access_token;
    mongoUser.grantingTimestamp = Math.floor(new Date() / 1000);
    mongoUser.save();

    return mongoUser.accessToken;
  },

  async seekTo(accessToken, position) {
    let data;
    try {
      data = await axios({
        url: `https://api.spotify.com/v1/me/player/seek?${
          queryString.stringify({
            position_ms: position,
          })}`,
        method: 'put',
        headers: {
          Authorization: systemService.getAuthHeader(accessToken),
        },
      });
    } catch (e) {
      throw Boom.badRequest('Trouble seeking track (spotify)', e.response.data.error);
    }

    if (data.status !== 204) { throw Boom.badImplementation(); }

    return 204;
  },
};
