import Session from '../models/Session';
import crypto from 'crypto';

export default {
  async createOrUpdateSessionForUser(userId) {
    const accessToken = this.createRandomToken();
    const session = await Session.findOne({ userId }).exec();

    if (session == null) {
      return Session.create({
        userId,
        accessToken,
      });
    }

    session.accessToken = accessToken;
    return session.save();
  },

  createRandomToken() {
    return crypto.randomBytes(64).toString('hex');
  },
};
