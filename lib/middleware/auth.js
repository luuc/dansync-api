/* eslint-disable */
import Boom from 'boom';
import userService from '../services/user';
import asyncMiddleware from './async';

export const authorizedForRoom = () => {
  return asyncMiddleware(async (req, res, next) => {
    try {
      const { authorization } = req.headers;

      if (authorization == null || typeof authorization === 'undefined') {
        throw Boom.unauthorized('No authorization provided');
      }

      const accessToken = authorization.replace('Bearer ', '');
      const { user, session } = await userService.findOneByAccessToken(accessToken);

      req.session = session;
      req.user = user;

      next();
    } catch (err) {
      const { statusCode } = err.output;

      if (statusCode === 401) {
        req.user = {
          authenticated: false,
        };
        next();
        return;
      }
      next(err);
    }
  });
};
