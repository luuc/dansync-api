import Boom from 'boom';

import asyncMiddleware from '../middleware/async';

import Room from '../models/Room';
import Meta from '../models/Meta';
import RoomManager from '../services/RoomManager';

export default (router) => {
  router.post('/meta/:pincode', asyncMiddleware(async (req, res) => {
    const { energy, danceability, acousticness } = req.body;
    const { pincode } = req.params;

    console.log('acousticness', acousticness);

    console.log(pincode);

    const room = await Room.find({ pincode }).exec();
    if (room.length > 1) {
      throw Boom.badImplementation('pincode already exists, please create another room.');
    }
    if (room.length === 0) {
      throw Boom.badRequest('room does not exist');
    }


    // TODO: make this work for multiple devices
    // TODO: get data in synchronously
    // check all info from
    const lastMeta = await Meta.find().sort({ _id: -1 }).limit(1).exec();

    try {
      await Meta.create({
        roomId: room[0].id,
        energy,
        danceability,
        acousticness,
        timestamp: Math.floor(new Date() / 1000),
      });
    } catch (e) {
      throw Boom.badRequest(e);
    }

    if (lastMeta == null) {
      res.json({
        inSync: 0,
      });
      return;
    }


    let inSync = 0;
    if (Math.abs(lastMeta[0].energy - energy) < 2) {
      inSync = (2.0 - Math.abs(lastMeta[0].energy - energy)) / 2;
    }

    res.json({
      inSync,
    });
  }));
  router.get('/meta/:pincode/sync', asyncMiddleware(async (req, res) => {
    const { pincode } = req.params;

    const room = await Room.find({ pincode }).exec();
    if (room.length > 1) {
      throw Boom.badImplementation('pincode already exists, please create another room.');
    }
    if (room.length === 0) {
      throw Boom.badRequest('room does not exist');
    }

    room[0].isListening = true;
    room[0].save();

    RoomManager.setTimer(room[0].id, 20000);

    res.sendStatus(200);
  }));
};
