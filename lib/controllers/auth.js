import Boom from 'boom';
import axios from 'axios';

import asyncMiddleware from '../middleware/async';

import roomService from '../services/rooms';
import systemService from '../services/system';
import spotifyService from '../services/spotify';
import userService from '../services/user';
import sessionService from '../services/session';

import UserIdentifier from '../models/UserIdentifier';
import Room from '../models/Room';

export default (router) => {
  router.get('/rooms/:roomId/authorize', asyncMiddleware(async (req, res) => {
    const room = await roomService.getRoomById(req.params.roomId);

    if (room == null) { throw Boom.NotFound('Room not found'); }

    res.json(spotifyService.getRedirect(room.id));
  }));

  router.post('/rooms/new', asyncMiddleware(async (req, res) => {
    const { roomName } = req.body;

    if (roomName == null) { throw Boom.badRequest('No room name provided'); }

    const getRandom = () => {
      return Math.floor(Math.random() * 10);
    };

    const room = await Room.create({
      name: roomName,
      isAvailable: false,
      isLisening: false,
      pincode: `${getRandom()}${getRandom()}${getRandom()}${getRandom()}${getRandom()}${getRandom()}`,
    });

    res.json(spotifyService.getRedirect(room.id));
  }));

  router.get('/authorization', asyncMiddleware(async (req, res) => {
    const spotifyCode = req.query.code;
    const roomId = req.query.state;

    if (spotifyCode == null || roomId == null) { throw Boom.badRequest(); }

    let spotifyAuth;

    try {
      spotifyAuth = await axios({
        url: 'https://accounts.spotify.com/api/token',
        method: 'post',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: systemService.getAuthHeader(),
        },
        params: {
          grant_type: 'authorization_code',
          code: spotifyCode,
          redirect_uri: `${process.env.WEB}/authorization`,
        },
      });
    } catch (e) {
      throw Boom.badRequest(e.response.data.error);
    }

    if (spotifyAuth.data.access_token == null) { throw Boom.badRequest('Cannot get spotify access'); }
    if (spotifyAuth.data.refresh_token == null) { throw Boom.badRequest('Cannot get spotify access'); }

    const userProfile = await userService.getMe(spotifyAuth.data.access_token);
    const room = await Room.findOne({ _id: roomId });

    let user;
    user = await UserIdentifier.findOne({ spotifyId: userProfile.id }).exec();

    if (user == null) {
      user = await UserIdentifier.create({
        accessToken: spotifyAuth.data.access_token,
        refreshToken: spotifyAuth.data.refresh_token,
        email: userProfile.email,
        spotifyId: userProfile.id,
      });
    }

    const session = await sessionService.createOrUpdateSessionForUser(user.id);

    room.isAvailable = true;
    room.save();

    // frontend will redirect to room
    res.json({
      accessToken: session.accessToken,
      userId: user.id,
      roomId: room.id,
      spotifyAccessToken: spotifyAuth.data.access_token,
    });
  }));
};
