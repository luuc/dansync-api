import Boom from 'boom';

import asyncMiddleware from '../middleware/async';

import spotifyService from '../services/spotify';
import { authorizedForRoom } from '../middleware/auth';

export default (router) => {
  router.get('/spotify/devices', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    const { user } = req;
    const devices = await spotifyService.getPlayers(user.accessToken);

    res.json({
      data: {
        devices,
      },
    });
  }));

  router.post('/spotify/devices', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    const { user } = req;

    if (req.body.deviceId == null) { throw Boom.badRequest(); }
    const data = await spotifyService.setPlayer(user.accessToken, req.body.deviceId);

    res.send(data.status);
    // res.send(204);
  }));

  router.get('/spotify/playlists', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    const { user } = req;
    const data = await spotifyService.getUserPlaylists(user.accessToken);

    res.json({ data });
    // res.send(204);
  }));
};
