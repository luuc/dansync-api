import Boom from 'boom';

import asyncMiddleware from '../middleware/async';

import roomService from '../services/rooms';
import spotifyService from '../services/spotify';
import UserIdentifier from '../models/UserIdentifier';
import Connection from '../models/Connection';

export default (router) => {
  router.delete('/connection', asyncMiddleware(async (req, res) => {
    const room = await roomService.getRoomById(req.query.roomId);
    const user = await UserIdentifier.findOne({ _id: req.query.userId }).exec();

    if (room == null) { throw Boom.notFound('Room not found'); }
    if (user == null) { throw Boom.notFound('User not found'); }

    const deleteStatus = await Connection.deleteOne({ roomId: room.id, userId: user.id }).exec();
    if (deleteStatus.result.ok !== 1) { throw Boom.badRequest('Cannot delete connection'); }

    res.sendStatus(204);
  }));

  router.post('/connection', asyncMiddleware(async (req, res) => {
    const room = await roomService.getRoomById(req.query.roomId);
    const user = await UserIdentifier.findOne({ _id: req.query.userId }).exec();

    const { playlistId } = req.body;

    if (room == null) { throw Boom.notFound('Room not found'); }
    if (user == null) { throw Boom.notFound('User not found'); }
    if (playlistId == null) { throw Boom.badRequest('Please provide a playlist'); }

    const connection = await Connection.findOne({ roomId: room.id, userId: user.id }).exec();
    connection.playlist = playlistId;
    connection.save();

    const playlist = await spotifyService.getInfoFromPlaylist(user.accessToken, playlistId);

    res.json(playlist);
  }));
};
