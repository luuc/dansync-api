import Boom from 'boom';

import asyncMiddleware from '../middleware/async';

import Room from '../models/Room';
import Connection from '../models/Connection';
import roomService from '../services/rooms';
import userService from '../services/user';
import spotifyService from '../services/spotify';
import systemService from '../services/system';

import { authorizedForRoom } from '../middleware/auth';

export default (router) => {
  router.get('/rooms', asyncMiddleware(async (req, res) => {
    const rooms = await Room.find().exec();

    res.json({
      data: {
        rooms: [
          ...rooms,
        ],
      },
    });
  }));

  router.get('/rooms/:roomId/tracks/current', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    const room = await Room.findOne({ _id: req.params.roomId }).exec();

    if (room == null) { throw Boom.notFound(); }

    if (room.currentTrack == null) {
      res.json({
        currentTrack: null,
      });
      res.end();
      return;
    }

    res.json({
      currentTrack: room.currentTrack.tracks[0],
      // TODO add owner
    });
  }));

  router.get('/rooms/:roomId/:userId/playlist', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    // only shows playlist of authenticated user
    const { user } = req;
    const connection = await Connection.findOne({ roomId: req.params.roomId, userId: user.id }).exec();

    if (connection == null) { throw Boom.notFound(); }
    if (connection.playlist == null) {
      res.sendStatus(200);
      return;
    }

    const playlist = await spotifyService.getSongsFromPlaylist(user.accessToken, connection.playlist);

    res.json(playlist);
  }));

  router.put('/rooms/:roomId/device', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    console.log('updating devices...');
    const { user } = req;
    const { roomId } = req.params;
    const { deviceId } = req.body;

    const room = await Room.findOne({ _id: roomId }).exec();

    if (room == null) { throw Boom.notFound('Requested room does not exist'); }
    if (deviceId == null) { throw Boom.badRequest(); }

    const connection = await Connection.findOne({ userId: user.id, roomId });
    const resp = await spotifyService.setPlayer(user.accessToken, deviceId);

    console.log(resp);

    if (connection.deviceId == null) {
      if (room.currentTrack == null) {
        await systemService.triggerNextSong(room.id, 0);
      } else {
        console.log('playCurrentRoomTrack');
        await roomService.playCurrentRoomTrack(user.accessToken, room);
      }
    } // else spotify handles the switch

    connection.deviceId = deviceId;
    connection.save();

    // TODO: if player is updated play new tracks

    res.sendStatus(resp.status);
  }));

  router.get('/rooms/pin/:pincode', asyncMiddleware(async (req, res) => {
    const { pincode } = req.params;
    console.log(pincode === '080315');

    const room = await Room.find({ pincode }).exec();
    console.log(room);
    if (room.length > 1) {
      throw Boom.badImplementation('pincode already exists, please create another room.');
    }
    if (room.length === 0) {
      throw Boom.badRequest('room does not exist');
    }

    res.json({
      data: {
        room: room[0],
      },
    });
  }));

  router.get('/rooms/:roomId', authorizedForRoom(), asyncMiddleware(async (req, res) => {
    const { user } = req;
    const { roomId } = req.params;

    if (roomId == null) { throw Boom.notFound('No room provided'); }

    const room = await Room.findOne({ _id: roomId }).exec();
    if (room == null) { throw Boom.notFound('Requested room does not exist'); }

    if (user.authenticated === false) {
      res.json(spotifyService.getRedirect(room.id));
      res.end();
      return;
    }

    let accessToken = user.accessToken;

    // if spotify token expired, refresh it
    if (user.grantingTimestamp < Math.floor(new Date() / 1000) - 1800) {
      accessToken = await spotifyService.refreshToken(user);
    }

    const users = await roomService.getRoomUsers(room.id);

    await roomService.connectUserToRoom(user.id, roomId);

    if (users == null) { throw Boom.notFound(); }

    const contentPromises = await users.map(async (user) => {
      let userProfile;

      try {
        userProfile = await userService.getMeWithFrontData(user);
      } catch (e) {
        return null;
      }

      return {
        user: {
          ...userProfile,
          mcId: user.id,
        },
      };
    });

    const content = await Promise.all(contentPromises);

    res.json({
      data: {
        accessToken, // spotify access token
        room,
        content,
      },
    });
  }));
};
