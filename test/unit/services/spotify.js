/* eslint-disable import/first */
import '../../mocks/mockgoose';

import chai, { expect } from 'chai';
import chaiSubset from 'chai-subset';
import dirtyChai from 'dirty-chai';

import spotifyService from '../../../lib/services/spotify';
import UserIdentifier from '../../../lib/models/UserIdentifier';
import Room from '../../../lib/models/Room';
import Meta from '../../../lib/models/Meta';

chai.use(chaiSubset);
chai.use(dirtyChai);

const ACCESS_TOKEN = 'BQC9Hrv6QjO0uC653cyVc0qdTERfW-hbyJzpDIMnsdBHxJCfJSsoW18O03pXYp9b-06YFqxAfnn3WFPaWcFejpJvm9STHQg-7fmdTZUDoX4sQ-r8lzEukeGvBh9IMMMCVZMpQh9Y2ghbGRP5X5tHKyU9P3dbO-wHUQXA1MjgRGd_OH-SVl_PYQ';

describe('Spotify Service', () => {
  describe('aggregateDiscoverWeekly', () => {
    it('single user in a room', async () => {
      const user = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const result = await spotifyService.aggregateDiscoverWeekly([user.id]);

      expect(result).to.exist();
      expect(result.length).to.equal(50);
    });
  });
  describe('playNextSongForRoom()', () => {
    it('single user in a room', async () => {
      const room = await Room.create({
        name: 'Test Room',
        connectionStrength: 3,
        isListening: true,
      });

      const user = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const timestamp = Math.floor(new Date() / 1000);

      await Meta.create({ value: 80, userId: user.id, roomId: room.id, timestamp });
      await Meta.create({ value: 30, userId: user.id, roomId: room.id, timestamp });
      await Meta.create({ value: 40, userId: user.id, roomId: room.id, timestamp });

      const result = await spotifyService.playNextSongForRoom(room.id);
      expect(result).to.exist();
    });
  });
});
