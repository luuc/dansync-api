/* eslint-disable import/first */
import '../../mocks/mockgoose';

import chai, { expect } from 'chai';
import chaiSubset from 'chai-subset';
import dirtyChai from 'dirty-chai';

import Room from '../../../lib/models/Room';

import metaService from '../../../lib/services/meta';
import UserIdentifier from '../../../lib/models/UserIdentifier';
import Meta from '../../../lib/models/Meta';

const ACCESS_TOKEN = 'BQC9Hrv6QjO0uC653cyVc0qdTERfW-hbyJzpDIMnsdBHxJCfJSsoW18O03pXYp9b-06YFqxAfnn3WFPaWcFejpJvm9STHQg-7fmdTZUDoX4sQ-r8lzEukeGvBh9IMMMCVZMpQh9Y2ghbGRP5X5tHKyU9P3dbO-wHUQXA1MjgRGd_OH-SVl_PYQ';

chai.use(chaiSubset);
chai.use(dirtyChai);

describe('Meta Service', () => {
  describe('getAggregatedValueFromMeta', () => {
    it('single user in a room', async () => {
      const room = await Room.create({
        name: 'Test Room',
        connectionStrength: 3,
        isListening: true,
      });

      const user = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const timestamp = Math.floor(new Date() / 1000);

      await Meta.create({ value: 80, userId: user.id, roomId: room.id, timestamp });
      await Meta.create({ value: 30, userId: user.id, roomId: room.id, timestamp });
      await Meta.create({ value: 40, userId: user.id, roomId: room.id, timestamp });

      const result = await metaService.getAggregatedMetadata(room.id);

      expect(result.length).to.equal(1);
      expect(result[0]._id).to.equal(user.id);
      expect(result[0].count).to.equal(3);
    });
    it('multiple users in a room', async () => {
      const room = await Room.create({
        name: 'Test Room',
        connectionStrength: 3,
        isListening: true,
      });

      const userA = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const userB = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const userC = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const timestamp = Math.floor(new Date() / 1000);

      await Meta.create({ value: 80, userId: userA.id, roomId: room.id, timestamp });
      await Meta.create({ value: 30, userId: userB.id, roomId: room.id, timestamp });
      await Meta.create({ value: 40, userId: userC.id, roomId: room.id, timestamp });
      await Meta.create({ value: 80, userId: userA.id, roomId: room.id, timestamp });
      await Meta.create({ value: 30, userId: userB.id, roomId: room.id, timestamp });
      await Meta.create({ value: 40, userId: userC.id, roomId: room.id, timestamp });

      const result = await metaService.getAggregatedMetadata(room.id);

      expect(result.length).to.equal(3);
      expect(result[0].count).to.equal(2);
      expect(result[1].count).to.equal(2);
      expect(result[2].count).to.equal(2);
    });
  });
  describe('getNextSongForRoom', () => {
    it('get user ids', async () => {
      const room = await Room.create({
        name: 'Test Room',
        connectionStrength: 3,
        isListening: true,
      });

      const userA = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const userB = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const userC = await UserIdentifier.create({
        accessToken: ACCESS_TOKEN,
        refreshToken: 'foo',
        culteryId: 'foo',
      });

      const timestamp = Math.floor(new Date() / 1000);

      await Meta.create({ value: 80, userId: userA.id, roomId: room.id, timestamp });
      await Meta.create({ value: 30, userId: userB.id, roomId: room.id, timestamp });
      await Meta.create({ value: 40, userId: userC.id, roomId: room.id, timestamp });
      await Meta.create({ value: 80, userId: userA.id, roomId: room.id, timestamp });
      await Meta.create({ value: 30, userId: userB.id, roomId: room.id, timestamp });
      await Meta.create({ value: 40, userId: userC.id, roomId: room.id, timestamp });

      await metaService.getNextSongForRoom(room.id);

      expect();
    });
  });
});
