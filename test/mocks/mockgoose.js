import mongoose from 'mongoose';
import { Mockgoose } from 'mockgoose-fix';

import state from '../../lib/state';

state.isServerMocked = true;

mongoose.Promise = Promise;

const mockgoose = new Mockgoose(mongoose);
mockgoose.helper.setDbVersion('3.4.3');

before(async () => {
  await mockgoose.prepareStorage();
  await mongoose.connect('mongodb://example.com/TestingDB', { useMongoClient: true });
});

beforeEach(async () => {
  await mockgoose.helper.reset();
});

// after(async () => {
//   await mongoose.unmock();
// });

export default mockgoose;
