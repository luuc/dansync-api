const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const path = require('path');

module.exports = {
  entry: {
    js: './lib/app.js',
  },
  output: {
    filename: './app.js',
    path: path.join(__dirname, '/dist/'),
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      use: 'babel-loader',
      exclude: /node_modules/,
    }],
  },
  target: 'node',
  devtool: 'eval',
  node: {
    __dirname: true,
    __filename: true,
  },
  externals: {
    'sharp': 'commonjs sharp',
    uws: 'uws'
  },
};
